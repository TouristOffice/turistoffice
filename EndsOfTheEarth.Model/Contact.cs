﻿using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Contact : Value<Contact>
    {
        private readonly NonEmptyString _address = new NonEmptyString("address");
        private readonly NonEmptyString _phone   = new NonEmptyString("phone");


        public string Address
        {
            get { return _address.Value; }
        }

        public string Phone
        {
            get { return _phone.Value; }
        }


        public Contact(string address, string phone)
        {
            _address.Value = address;
            _phone.Value = phone;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { Address, Phone };
        }
    }
}
