﻿using System;
using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Tour : Entity
    {
        private NonEmptyString _name = new NonEmptyString("name");

        public Hotel Hotel { get; set; }

        private readonly HashSet<TourItem> _items = new HashSet<TourItem>();

        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public ICollection<TourItem> TourItems
        {
            get { return _items; }
        }
  
        public Tour(Guid id, string name, Hotel hotel)
            :   base( id )
        {
            Name = name;
            Hotel = hotel;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}",
                       Id,
                       Name
                   );
        }
    }
}
