﻿
namespace EndsOfTheEarth.Model
{
    public enum OrderStatus
    {
        Placed,
        Confirmed,
        Cancelled,
        Completed
    }
}
