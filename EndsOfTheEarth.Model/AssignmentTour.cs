﻿using System;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class AssignmentTour: Entity
    {
        private readonly RequiredProperty<Hotel> _hotel = new RequiredProperty<Hotel>("hotel");
        private readonly RequiredProperty<TourItem> _tourItem = new RequiredProperty<TourItem>("tourItem");


        public Country AssignedCountry
        {
            get { return _hotel.Value.Region.Country; }
        }
        
        public Region AssignedRegion
        {
            get { return _hotel.Value.Region; }
        }

        public Hotel AssignedHotel
        {
            get { return _hotel.Value; }
            private set { _hotel.Value = value; }
        }

        public TourItem AssignedItemTour
        {
            get { return _tourItem.Value; }
            private set { _tourItem.Value = value; }
        }


        public AssignmentTour(Guid id, Hotel hotel, TourItem tourItem)
            :base(id)
        {
            _hotel.Value = hotel;
            _tourItem.Value = tourItem;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nCountry = {1}\nRegion = {2}\nHotel = {3}\n Item tour:\n{4}",
                       Id,
                       AssignedCountry.Name,
                       AssignedRegion.Name,
                       AssignedHotel.Name,
                       AssignedItemTour
                   );
        }
    }
}
