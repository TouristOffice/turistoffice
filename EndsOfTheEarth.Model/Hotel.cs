﻿using System;
using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Hotel: Entity
    {
        private NonEmptyString _name = new NonEmptyString("name");
        private NonEmptyString _description = new NonEmptyString("description");
        private RangeProperty<int> _category = new RangeProperty<int>("category", 0, false, 5, true);

        public Region Region { get; private set; }
        public ICollection<Service> ListService { get; set; } = new List<Service>();

        public string ImageUrl { get; set; }

        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public int Category
        {
            get { return _category.Value; }
            set { _category.Value = value; }
        }

        public string Description
        {
            get { return _description.Value; }
            set { _description.Value = value; }
        }

        public Hotel(Guid id, Region region, string name, string description, int category)
            :base(id)
        {
            Region = region;
            _name.Value = name;
            _description.Value = description;
            _category.Value = category;
            ImageUrl = "";
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}\nCategory = {2}\nImageUrl = {3}\nDescription = {4}",
                       Id,
                       Name,
                       Category,
                       ImageUrl,
                       Description
                   );
        }
    }
}
