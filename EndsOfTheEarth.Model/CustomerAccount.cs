﻿using System;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    class CustomerAccount: Account
    {
        private RequiredProperty<Contact> _contact = new RequiredProperty<Contact>("contact");

        public Contact Contact
        {
            get { return _contact.Value; }
        }
        
        public CustomerAccount(Guid id, string name, string email, string passwordHash, Contact contact)
            : base(id, name, email, passwordHash)
        {
            _contact.Value = contact;
        }
    }
}
