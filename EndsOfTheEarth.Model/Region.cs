﻿using System;
using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Region: Entity
    {
        private NonEmptyString _name = new NonEmptyString("name");

        public Country Country { get; private set; }
        public ICollection<Tour> ListTour { get; private set; }

        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public Region(Guid id, Country country, string name)
            :base(id)
        {
            Country = country;
            _name.Value = name;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}\n",
                       Id,
                       Name
                   );
        }
    }
}
