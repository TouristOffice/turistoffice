﻿using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Discount: Value<Discount>
    {
        private readonly RangeProperty<decimal> _percent =
            new RangeProperty<decimal>("value", 0.0M, true, 100.0M, true);


        public decimal Percent
        {
            get { return _percent.Value; }
        }


        public Discount()
        {
            _percent.Value = 0.0M;
        }

        public Discount(decimal value)
        {
            _percent.Value = value;
        }

        public override string ToString()
        {
            return Percent.ToString();
        }

        public decimal GetDiscountedPrice(decimal price)
        {
            return price * (100.00M - Percent) / 100.00M;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { Percent };
        }
    }
}
