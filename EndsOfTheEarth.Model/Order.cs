﻿using System;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Order: Entity
    {        
        private readonly RequiredProperty<AssignmentTour> _assignmentTour 
            = new RequiredProperty<AssignmentTour>("assignmentTour");
        

        public Discount AssignedDiscount { get; private set; } = new Discount();

        public OrderStatus Status { get; private set; }

        public DateTime PlacementTime { get; private set; }

        public decimal BasicCost { get; private set; }

        public decimal TotalCost
        {
            get { return AssignedDiscount.GetDiscountedPrice(BasicCost); }
        }


        public Order(Guid id, DateTime time, AssignmentTour assignmentTour)
            : base(id)
        {
            Status = OrderStatus.Placed;
            PlacementTime = time;
            _assignmentTour.Value = assignmentTour;
            BasicCost = _assignmentTour.Value.AssignedItemTour.Price;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nBasic cost = {1}\nDiscount = {2}%\nTotal cost = {3}\nStatus = {4}\nPlaced = {5}",
                       Id,
                       BasicCost,
                       AssignedDiscount,
                       TotalCost,
                       Status,
                       PlacementTime
                   );
        }

        public void SetDiscount(Discount discount)
        {
            if (discount == null)
                throw new ArgumentNullException("Order.SetDiscount: null discount object");

            AssignedDiscount = discount;
        }

        public void Confirm()
        {
            if (Status != OrderStatus.Placed)
                throw new InvalidOperationException("Order.Confirm - can only run in Placed state");

            Status = OrderStatus.Confirmed;
        }

        public void Cancel()
        {
            if (Status != OrderStatus.Placed)
                throw new InvalidOperationException("Order.Cancel - can only run in Placed state");

            Status = OrderStatus.Cancelled;
        }

        public void Completed()
        {
            if (Status != OrderStatus.Placed)
                throw new InvalidOperationException("Order.Completed - can only happen in  state");

            Status = OrderStatus.Completed;
        }
    }
}
