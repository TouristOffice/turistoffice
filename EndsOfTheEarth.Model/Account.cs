﻿using System;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public abstract class Account : Entity
    {
        private NonEmptyString  _name     = new NonEmptyString("name");
        private RegexString     _email    = new RegexString("email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
        private NonEmptyString  _password = new NonEmptyString("password");


        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public string Email
        {
            get { return _email.Value; }
            set { _email.Value = value; }
        }


        public Account(Guid id, string name, string email, string password)
            : base(id)
        {
            Name = name;
            Email = email;
            ChangePassword(password);
        }

        public bool CheckPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            return _password.Value == password;
        }

        public void ChangePassword(string password)
        {
            _password.Value = password;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}\nEmail = {2}\nPassword = {3}",
                       Id,
                       Name,
                       Email,
                       _password.Value
                   );
        }
    }
}
