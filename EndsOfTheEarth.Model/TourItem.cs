﻿using System;
using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class TourItem: Value<TourItem>
    {
        private readonly RangeProperty<int> _quantityNight =
            new RangeProperty<int>("quantity", 0, false, int.MaxValue, true);

        private readonly RangeProperty<decimal> _price =
            new RangeProperty<decimal>("fixedPrice", 0, true, decimal.MaxValue, true);


        public DateTime DepartureDate { get; private set; }

        public int QuantityNight
        {
            get { return _quantityNight.Value; }
        }

        public decimal Price
        {
            get { return _price.Value; }
        }

        public TourItem(int quantityNight, DateTime departureDate, decimal price)
        {
            if (quantityNight <= 0)
                throw new Exception("TourItem: non-positive quantity");

            _price.Value = price;
            _quantityNight.Value = quantityNight;
            DepartureDate = departureDate;
        }

        public override string ToString()
        {
            return string.Format(
                       "Departure date = {0}, Quantity night = {1}, Price = {2}",
                       DepartureDate,
                       QuantityNight,
                       Price                       
                   );
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { DepartureDate, QuantityNight, Price };
        }
    }
}
