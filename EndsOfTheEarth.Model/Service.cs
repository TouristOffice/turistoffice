﻿using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Service: Value<Service>
    {
        private readonly NonEmptyString _name = new NonEmptyString("name");

        public string Name
        {
            get { return _name.Value; }
        }
        

        public Service(string name)
        {
            _name.Value = name;
        }

        public override string ToString()
        {
            return Name;
        }

        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { Name };
        }
    }
}
