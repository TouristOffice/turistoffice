﻿using System;
using System.Collections.Generic;
using TouristOffice.Utils;
using TouristOffice.Utils.Property;

namespace EndsOfTheEarth.Model
{
    public class Country: Entity
    {
        private NonEmptyString _name = new NonEmptyString("name");

        public string About { get; set; }

        public ICollection<Tour> ListRegion { get; private set; }
        
        public string Name
        {
            get { return _name.Value; }
            set { _name.Value = value; }
        }

        public Country(Guid id, string name)
            :base(id)
        {
            _name.Value = name;
            About = "";
        }



        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nName = {1}\nAbout = {2}\n",
                       Id,
                       Name,
                       About
                   );
        }
    }
}
