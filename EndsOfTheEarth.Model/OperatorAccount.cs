﻿using System;
using System.Collections.Generic;

namespace EndsOfTheEarth.Model
{
    public class OperatorAccount : Account
    {
        private readonly List<Order> _orders;

        public ICollection<Order> Orders
        {
            get { return _orders.AsReadOnly(); }
        }

        public OperatorAccount(Guid id, string name, string email, string passwordHash)
            : base(id, name, email, passwordHash)
        {
            _orders = new List<Order>();
        }

        public void TrackOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orders.Add(order);
        }
    }
}
