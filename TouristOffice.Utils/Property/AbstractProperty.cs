﻿using System;

namespace TouristOffice.Utils.Property
{
    public abstract class AbstractProperty
    {
        public string ParamName { get; private set; }

        protected AbstractProperty(string paramName)
        {
            if (paramName == null)
                throw new ArgumentNullException("paramName");

            ParamName = paramName;
        }
    }
}
