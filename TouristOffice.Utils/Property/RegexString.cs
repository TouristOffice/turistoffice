﻿using System;
using System.Text.RegularExpressions;

namespace TouristOffice.Utils.Property
{
    public class RegexString : NonEmptyString
    {
        private readonly string pattern;

        public RegexString(string name, string pattern)
            : base(name)
        {
            this.pattern = pattern;
        }

        protected override void CheckValue(string value)
        {
            base.CheckValue(value);
            if (!Regex.IsMatch(value, pattern))
                throw new ArgumentException("Invalid format", ParamName);
        }
    }
}
