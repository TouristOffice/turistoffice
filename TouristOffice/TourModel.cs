﻿using EndsOfTheEarth.Model;
using System.Collections.Generic;

namespace TouristOffice
{
    public class TourModel
    {
        public ICollection<Account> Accounts { get; private set; } = new List<Account>();
        public ICollection<Country> Countries { get; private set; } = new List<Country>();
        public ICollection<Region> Regions { get; private set; } = new List<Region>();
        public ICollection<Hotel> Hotels { get; private set; } = new List<Hotel>();
        public ICollection<Tour> Tours { get; private set; } = new List<Tour>();
        public ICollection<AssignmentTour> AssignmentTours { get; private set; } = new List<AssignmentTour>();
        public ICollection<Order> Orders { get; private set; } = new List<Order>();
    }
}
