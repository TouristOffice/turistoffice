﻿using System;
using System.IO;

namespace TouristOffice
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TourModel model = TestModelGenerator.GenerateTestData();

                ModelReporter reporter = new ModelReporter(Console.Out);
                reporter.GenerateReport(model);                                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType().FullName);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
