﻿using EndsOfTheEarth.Model;
using System;
using System.Linq;

namespace TouristOffice
{
    public static class TestModelGenerator
    {
        private static AssignmentTour _assignmentTour;
        private static Country _country;
        private static Region _region;
        private static Hotel _hotel;
        private static Tour _tour;
        private static Order _order;


        public static TourModel GenerateTestData()
        {
            TourModel m = new TourModel();

            GenerateAccounts(m);
            GenerateCountries(m);
            GenerateRegions(m);
            GenerateHotels(m);
            GenerateTours(m);
            GenerateAssignmentTours(m);
            GenerateOrders(m);

            return m;
        }


        private static void GenerateAccounts(TourModel m)
        {
            OperatorAccount lusya = new OperatorAccount(Guid.NewGuid(), "Lusya Petrova", "lusya.gorna@tour.com", "qwerty");
            m.Accounts.Add(lusya);
        }

        private static void GenerateCountries(TourModel m)
        {
            _country = new Country(Guid.NewGuid(), "Austria");
            _country.About = "...";

            m.Countries.Add(_country);
        }

        private static void GenerateRegions(TourModel m)
        {
            _region = new Region(Guid.NewGuid(), _country, "Vena");
            m.Regions.Add(_region);
        }

        private static void GenerateHotels(TourModel m)
        {
            _hotel = new Hotel(Guid.NewGuid(), _region, "Ogor", "...", 5);
            _hotel.ImageUrl = "http://EndsOfTheEarth.com/uploads/img/1280x620/ogor.jpg";
            m.Hotels.Add(_hotel);

            _hotel.ListService.Add(new Service("mini-bar"));
            _hotel.ListService.Add(new Service("food"));
            _hotel.ListService.Add(new Service("bar"));
            _hotel.ListService.Add(new Service("restoran"));
            _hotel.ListService.Add(new Service("safe"));
        }

        private static void GenerateTours(TourModel m)
        {
            _tour = new Tour(Guid.NewGuid(), "Roged", _hotel);

            _tour.TourItems.Add(new TourItem(5, new DateTime(2016, 6, 12), 300));
            _tour.TourItems.Add(new TourItem(7, new DateTime(2016, 5, 5), 250));
            _tour.TourItems.Add(new TourItem(9, new DateTime(2016, 6, 12), 300));

            _tour.Hotel = _hotel;
            m.Tours.Add(_tour);
        }

        private static void GenerateAssignmentTours(TourModel m)
        {
            var assignmentTour = _tour.TourItems.ToList();
            _assignmentTour = new AssignmentTour(Guid.NewGuid(), _tour.Hotel, assignmentTour[0]);
            m.AssignmentTours.Add(_assignmentTour);
        }

        private static void GenerateOrders(TourModel m)
        {
            _order = new Order(Guid.NewGuid(), new DateTime(2016, 4, 10), _assignmentTour);
            m.Orders.Add(_order);
            
            _order.SetDiscount(new Discount(10.0M));
        }

    }
}
