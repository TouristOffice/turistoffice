﻿using System.Collections.Generic;
using System.IO;

namespace TouristOffice
{
    public class ModelReporter
    {
        private TextWriter output;


        public ModelReporter(TextWriter output)
        {
            this.output = output;
        }

        public void GenerateReport(TourModel model)
        {
            ReportCollection("Accounts", model.Accounts);
            ReportCollection("Countries", model.Countries);
            ReportCollection("Regions", model.Regions);
            ReportCollection("Hotels", model.Hotels);
            ReportCollection("Tours", model.Tours);
            ReportCollection("AssignmentTours", model.AssignmentTours);
            ReportCollection("Orders", model.Orders);
        }

        private void ReportCollection<T>(string title, ICollection<T> items)
        {
            output.WriteLine("==== {0} ==== ", title);
            output.WriteLine();

            foreach (var item in items)
            {
                output.WriteLine(item);
                output.WriteLine();
            }

            output.WriteLine();
        }
    }
}
